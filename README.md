# ESICUP Challenge 2015 - Statement and Data #

ESICUP Challenge 2015 is about 3D Multi-Container 
Loading Problem on the data supplied by RENAULT.

[Web Page with Benchmark](http://benchmark.gent.cs.kuleuven.be/mclp/en/).

## Repository Organization

- `competition.pdf` - Description of the challenge problem.
- `format.pdf` - Describes the format of the data.
- `instances.zip` - Archive with instances.
- `instancesA` - Set of instances A (qualification phase).
- `instancesB` - Set of instances B (final phase).
- `instancesX` - Set of instances X (on-site phase).
- `visualization.zip` - Software for result visualization.
- `checker.zip` - Checker to verify if the solution is correct.
- `README_Checker.rtf` - Instructions for checker.
- `README_Visualiation.rtf` - Instructions for visualizator.
